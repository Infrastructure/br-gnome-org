# br.gnome.org

Brazilian GNOME community website

## Testando

Para testar o site localmente, em seu próprio computador, faça o seguinte:

1. Instale o [Hugo][Link para o Hugo] via pacote de sua distribuição ou manualmente; se manualmente:
    1. Baixe o [tarball][Pagina de download do tarball do Hugo]
    2. Descompacte o tarball (ex., `tar -xvzf ~/Downloads/hugo_X.Y_Linux-64bit.tgz`)
    3. Copie o arquivo executável hugo para `/home/seu-usuario/bin` (ou outra pasta que esteja no PATH)
    4. Verifique a instalação com o comando `hugo version`
2. Na pasta raiz do projeto, execute o comando `hugo server`
3. Abra o navegador no endereço: http://localhost:1313/

[Link para o Hugo]: https://gohugo.io/getting-started/installing
[Pagina de download do tarball do Hugo]: https://github.com/gohugoio/hugo/releases

