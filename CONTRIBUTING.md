# Contribuindo para o br.gnome.org

> This file is written in Portuguese as probably most of the readers
are Brazilians. If that is not your case and you want to read in
English, please see [CONTRIBUTING-EN.md]

Obrigado pelo seu interesse em nosso projeto!

Aqui você encontra algumas informações para guiá-lo no processo de
participar da construção e manutenção do página web da comunidade
brasileira do GNOME, seja relatando erros, solicitando alguma
alteração ou até mesmo submetendo propostas de alterações.

## Como posso ajudar

Se você ainda não nos contatou, sinta-se à vontade para fazê-lo nos
meios de comunicação abaixo.

Seguem aqui alguns recursos:
 - Lista de discussões para assuntos gerais:
    [gnome-br-list](https://mail.gnome.org/mailman/listinfo/gnome-br-list)
 - Lista de discussão para tradução:
    [gnome-pt_br-list](https://mail.gnome.org/mailman/listinfo/gnome-pt_br-list)
 - IRC: [#gnome-br em irc.gimp.org](irc://irc.gnome.org/gnome-br)
 - Erros ou melhorias: Relate uma
    [issue](https://gitlab.gnome.org/Infrastructure/br.gnome.org/issues) 
    ou proponha uma
    [merge request](https://gitlab.gnome.org/Infrastructure/br.gnome.org/merge_requests)

### Submetendo alterações

Preferencialmente, use o sistema de **merge request** do GitLab, seguindo as
convenções de mensagens de commit do
[Guidelines for Commit Messages](https://wiki.gnome.org/Git/CommitMessages),
para apresentar alguma proposta de alteração ao código-fonte. Caso não esteja
familiarizado com esse recurso, pode utilizar outro dos recursos listados acima.

Ao propor _merge requests_, certifique-se que seus _commits_ sigam 

Em especial, mantenha seus commits atômicos (um recurso por commit) e sempre
escreva uma mensagem de log limpa e clara em seus commits. Mensagens de uma
linha são ótimas para pequenas alterações, mas grandes alterações ficam
melhor descritas dessa forma:

```
 $ git commit -m "Exemplifica uma mensagem breve de commit
 >
 > Um parágrafo descrevendo o que foi alterado neste commit e seu impacto
 > de forma que seja sempre possível lembrar o porquê dessa alteração daqui
 > a alguns meses ou anos.
```


### Convenções de formatação

TODO