# Faça parte de uma comunidade vibrante e internacional {#top}

![Comunidade GNOME](../img/envolva-se-banner.png)

<p class="main-feature center">
A comunidade GNOME  é aberta e muito receptiva. Envolver-se é uma excelente maneira de aprender
novas habilidades, divertir-se e ajudar a criar Software Livre para o mundo inteiro.
</p>

# Página em construção

A comunidade brasileira do projeto GNOME utiliza os canais de IRC para se comunicar.
