# Página em construção

Os colaboradores brasileiros que participam e contribuem de alguma forma com esse projeto se reúnem
por meio da comunidade [GNOME Brasil](index.html). Da mesma forma como acontece em comunidades de
desenvolvedores e usuários do GNOME em outros países, a comunidade brasileira é aberta e muito
receptiva a novas(os) colaboradoras(es). Para saber como contribuir, veja a página de
[colaboração](Colabore.html). A comunicação diária entre os membros da comunidade brasileira ocorre
por meio de listas de discussão e IRC (veja abaixo).

Recomendamos também a leitura do [Código de Conduta](CodigoConduta.html),
para que tenha conhecimento das regras básicas de convivência em nossa
comunidade.

------------------------------------------------------------------------

## Páginas relacionadas {#paginas_relacionadas}

-   [Wiki do GNOME (BR)](https://wiki.gnome.org/GnomeBrasil)

<!-- -->

-   [Rede Software Livre Brasil](http://softwarelivre.org/projeto-gnome)

[comunidade_internacional]: https://wiki.gnome.org/GnomeWorldWide
