# Traduzindo o GNOME

O ambiente [GNOME], assim como diversos outros projetos de
software livre, possibilita a tradução de sua interface de usuário
(UI) e também documentações. Desta forma, uma vez traduzido, permite
que toda uma comunidade use os programas de forma mais confortável
em seu idioma nativo.

Todo esse trabalho de tradução é feita por voluntários de diversas
partes do mundo para cada idioma sob a coordenação do
[GNOME Translation Project (GTP)].

## Conteúdo
- [Vídeo introdutório]
- [Sobre a equipe brasileira de tradução]
- [Pendências e pontos de ataque]
- [Prioridades de tradução]
- [Como acompanho as atividades de tradução]
- [Como posso ajudar]
    - [Relatando um erro de escrita na tradução]
    - [Sugerindo melhorias à tradução existente]
    - [Divulgando o trabalho de tradução]
    - [Traduzindo o GNOME]
- [Veja também]

----

## Vídeo introdutório

Você é uma pessoa mais visual e prefere ver vídeo? Então, confira essa apresentação no GNOME Latam 2021 com algumas informações introdutórias sobre o processo de tradução do GNOME.

{{< youtube id="ph4I-sGaoHk" >}}

----

## Sobre a equipe brasileira de tradução

Da mesma forma que outras equipes de tradução do GNOME, a equipe de
tradução para português brasileiro é composta de voluntários, e toda
contribuição é muito bem-vinda!

Veja os meios de contato conosco na [página inicial](/).

----

## Pendências e pontos de ataque

Listamos aqui alguns problemas que tenham que qualquer ajuda será
muito bem-vinda. Se deseja atuar em alguma dessas áreas, nos avise
e auxiliaremos bem como atuaremos juntos.

Surgiu a dúvida: “[Como posso ajudar]”? Além desta seção, veja mais
abaixo.

- **Captar mais contribuidores**  
Uma equipe com mais contribuidores possibilita uma tradução mais
rápida e mais bem debatida, possibilitando mais qualidade por meio
da revisão. Atualmente, não possuímos muitos membros ativos.  
Você tem ideias de como podemos trazer mais pessoas para traduzir o
GNOME? Tem ideias para facilitar o engajamento destes contribuidores?
nos fale!

- **GIMP, o programa de manipulador de imagens do GNU**  
GIMP estão precisando de atenção na tradução tanto da UI quanto da
documentação. Esta é uma tradução contendo termos bem especializados,
de forma que se você é usuário deste programa, melhor ainda!

- **GNOME Recipes, lista de receitas culinárias**  
O Receitas possui muitos termos especializados de culinária. Se você
tem facilidade com nomes em inglês de ingredientes e pratos, este
programa está precisando de atenção.

- **Gnumeric, programa de planilhas**  
Misturando termos especializados e outros nem tanto, este programa
requer consistência entre a tradução da interface gráfica (UI) e das
funções. Sim, são arquivos de tradução separados. Hoje a UI possui
algumas mensagens não traduzidas que merecem ser traduzidas, mas
mesmo as traduzidas merecem ser verificadas por consistência com a
tradução das funções.

- **Tradução da documentação dos programas**  
Sendo a prioridade traduzir a interface gráfica dos aplicativos, por
vezes a documentação acaba ficando com parcialmente ou não traduzida.
Há casos em que são traduções extensas que exigem carinho, tempo,
atenção e paciência.

----

## Prioridades de tradução

> **NOTA:** Os itens estão ordenados por ordem de prioridade.

**Prioridade em qualquer ocasião**
1. UI do [Damned Lies] e de outros itens contidos no rótulo
   “Infraestrutura do GNOME”

**Fora do período de congelamento que precede novo lançamento**  
1. Versões rotuladas com “Estável”, tradução que será sincronizada ao
   final com a versão atual por meio da funcionalidade *cherry-pick*
   do Git
2. UI ou Documentação, a seu critério

**Durante o período de congelamento que precede novo lançamento**
1. UI de programas na versão rotulada com “Desenvolvimento”
2. UI da programas na seção rotulada “Aplicativos Extras do GNOME”


----

## Como acompanho as atividades de tradução

Anteriormente, as equipes de tradução do GNOME tinham listas de discussão, a qual era utilizada receber notificações do [Damned Lies](https://l10n.gnome.org) bem como para nos comunicarmos. No entanto essas listas de discussão foram [descontinuadas](https://mail.gnome.org/archives/gnome-pt_br-list/2022-October/msg00002.html) em novembro de 2022.

O [histórico da lista de discussão gnome-pt_br-list](https://mail.gnome.org/archives/gnome-pt_br-list/) está disponível, apesar da lista estar em modo somente leitura.

Para acompanhar as atividades de tradução, uma forma é utilizar um agregador de RSS e aplicar o link de RSS da equipe brasileira de tradução:

1. Instale um agregador ([NewsFlash](https://gitlab.com/news-flash/news_flash_gtk), [Feeds](https://gitlab.gnome.org/World/gfeeds) e outros)
2. Configure a URL de feed RSS: https://l10n.gnome.org/rss/languages/pt_BR/

Com isso, você deve receber atualizações de todas as atividades deste idioma no Damned Lies.

Em caso de dúvidas ou sugestões, entre em contato nos meios listados na [página inicial](/).

----

## Como posso ajudar

Obrigado pelo seu interesse! A ajuda pode feita ser em várias formas.
Além das [pendências e pontos de ataque] acima, temos também algumas
instruções genéricas de como você pode ajudar, e elas são:

- [Relatando um erro de escrita na tradução]
- [Sugerindo melhorias à tradução existente]
- [Divulgando o trabalho de tradução]
- [Traduzindo o GNOME]

#### Relatando um erro de escrita na tradução

Somos humanos, então, apesar de nossos esforços, escapam erros de
escrita. Se você encontrou algum, nos avise e corrigiremos!

Antes, colete algumas informações para nos ajudar:
- Em qual programa você acredita estar o erro?
- Quais os passos para reproduzir/chegar até o erro?
- Qual a versão do GNOME e do programa em que está o erro?
- Capturas de tela (“screenshots”) que mostrem o erro, se necessário

Preencha e envie um novo *issue* no [rastreador de problemas da equipe
brasileira no GitLab do GNOME][pt_br_issues]. Caso prefira tirar
dúvida antes criar o *issue*, contate a equipe em algum dos meios
informados acima.

#### Sugerindo melhorias à tradução existente

Tem alguma sugestão de melhoria em alguma tradução? Nos avise! Faça
o seguinte:
- Onde conveniente, colete informações como mencionado na seção
  [acima](#relatando-um-erro-de-escrita)
- Sugeria fazendo uma comparação tipo “Onde tem AQUELE TEXTO,
  substituir por ESTE TEXTO”

Assim como na seção anterior, pedimos que preencha e envie um novo
*issue* no [rastreador de problemas da equipe brasileira no GitLab do
GNOME][pt_br_issues]. Caso prefira tirar dúvida antes de fazer uma
sugestão, contate a equipe em algum dos meios informados acima.

#### Divulgando o trabalho de tradução

A tradução do GNOME é feita com carinho, atenção, muito suor e com a
amada contribuição da comunidade. Estamos sempre abertos para novos
contribuidores, desde a mais simples correção de erro de escrita (que
é importante na experiência do usuário) a tradução de programas e
documentações não traduzidas.

Relatar erros de escrita? Opa, “bora”! Melhorar textos traduzidos?
Sim, por favor! Entrar na equipe e “derrubar” mensagens não
traduzidas? Melhor ainda!

Então, por favor, espalhe para os quatro cantos do mundo sobre a
tradução do GNOME. Divulgue que a equipe brasileira do GNOME está
aceitando contribuições na tradução! ;)

#### Traduzindo o GNOME

O fluxo de trabalho da tradução ocorre no [Damned Lies],
que é uma plataforma de tradução construída em Django. Concluído o
processo de tradução, a tradução é enviada ao repositório no [GitLab
do GNOME] para o respectivo projeto.

Utiliza-se o padrão [GNU Gettext], ou seja, a tradução é
feita em arquivos de extensão .po. Veja [sobre arquivos PO] para mais
informações.

Estes arquivos são traduzidos usando editores, especializados ou não.
Exemplos de editores especializados são [Poedit] e
[Gtranslator]. Porém, também servem editores comuns,
como [Gedit] ou [Vim].

----

## Veja também

- [GNOME Translation Project (GTP)], wiki do GNOME
- [Localisation Guide], por Translate House *(inglês)*

[Vídeo introdutório]: #vídeo-introdutório
[Sobre a equipe brasileira de tradução]: #sobre-a-equipe-brasileira-de-tradução
[Pendências e pontos de ataque]: #pendências-e-pontos-de-ataque
[Prioridades de tradução]: #prioridades-de-tradução
[Como acompanho as atividades de tradução]: #como-acompanho-as-atividades-de-traduo
[Como posso ajudar]: #como-posso-ajudar
[Relatando um erro de escrita na tradução]: #relatando-um-erro-de-escrita-na-tradução
[Sugerindo melhorias à tradução existente]: #sugerindo-melhorias-à-tradução-existente
[Divulgando o trabalho de tradução]: #divulgando-o-trabalho-de-tradução
[Traduzindo o GNOME]: #traduzindo-o-gnome
[Veja também]: #veja-também


[GNOME]: https://www.gnome.org
[GNOME Translation Project (GTP)]: https://wiki.gnome.org/TranslationProject
[Damned Lies]: https://l10n.gnome.org
[GitLab do GNOME]: https://gitlab.gnome.org
[gnome-pt_br-list]: https://mail.gnome.org/mailman/listinfo/gnome-pt_br-list
[pt_br_issues]: https://gitlab.gnome.org/Teams/Translation/pt_BR/issues
[GNU Gettext]: https://www.gnu.org/software/gettext
[sobre arquivos PO]: https://www.gnu.org/software/gettext/manual/html_node/PO-Files.html
[Poedit]: https://poedit.net/
[Gtranslator]: https://wiki.gnome.org/Apps/Gtranslator
[Gedit]: https://wiki.gnome.org/Apps/Gedit
[Vim]: https://www.vim.org/
[Localisation Guide]: http://docs.translatehouse.org/projects/localization-guide/en/latest/guide/start.html
