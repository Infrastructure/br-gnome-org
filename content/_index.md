![GNOME Banner](img/br-wgo-splash-40-1536x819.png)

# Descubra o GNOME

Um jeito fácil e elegante de usar seu computador, o GNOME é projetado para te colocar no controle
e fazer as coisas acontecerem.

[Saiba Mais](http://gnome.org)

# GNOME Brasil

Olá! Somos a comunidade brasileira do GNOME, uma comunidade ativa, aberta,
espalhada por diversas regiões do país e receptiva a novas(os) contribuidoras(es).
**Faça parte você também!**

Você pode entrar em contato pelos meios abaixo:

 * **Telegram**:
    * **Geral**: [@GNOMEBrasil](https://t.me/GNOMEBrasil)
    * **Tradução**: [@GNOMEBrasil_L10n](https://t.me/GNOMEBrasil_L10n)
 * **Matrix/IRC**: [#gnome-br:gnome.org no Matrix](https://matrix.to/#/#gnome-br:gnome.org) ou [\#gnome-br no Libera Chat](ircs://irc.libera.chat/gnome-br)
 * **GNOME Discourse**: [International/Brazilian Portuguese](https://discourse.gnome.org/c/international/brazilian-portuguese)
 * **Rastreador de problemas de tradução**: [Teams/Translation/pt_BR/issues]

Também estamos presentes nas redes sociais:
 
 * **Twitter**: [@gnome_br](https://twitter.com/gnome_br)
 * **Mastodon**: [@gnome_br](https://mastodon.social/@gnome_br)
 
Arquivos das listas de discussão (descontinuadas):

 * **geral**: [gnome-br-list]
 * **tradução**: [gnome-pt_br-list]

Saiba o que a comunidade brasileira está falando sobre o GNOME no agregador [**Planeta GNOME Brasil**](http://planeta.br.gnome.org/).

# Tradução

Veja nossa [documentação de tradução].

# GNOME pelo mundo

Por trás do Projeto GNOME existe uma [comunidade internacional][comunidade_internacional]
de voluntários e profissionais que projetam, programam, traduzem, documentam e
se divertem juntos.

# Código de Conduta

Veja o [Código de Conduta] do GNOME para as políticas para participar desta comunidade.

<!--
Gostaria de colaborar? Visite a nossa página de [colaboração](../colabore.html).
Aproveite a oportunidade e venha conversar um pouco conosco através de algum dos
nossos [meios de comunicação](../).
-->

[comunidade_internacional]: https://wiki.gnome.org/GnomeWorldWide
[gnome-br-list]: https://mail.gnome.org/archives/gnome-br-list/
[gnome-pt_br-list]: https://mail.gnome.org/archives/gnome-pt_br-list/
[Teams/Translation/pt_BR/issues]: https://gitlab.gnome.org/Teams/Translation/pt_BR/issues

[Documentação de tradução]: /traducao/
[Código de Conduta]: /codigo-de-conduta/
