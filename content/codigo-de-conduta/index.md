# Código de Conduta do GNOME

> **NOTE:** This is an unofficial Brazilian Portuguese translation of GNOME Code of Conduct. The official text can be found in https://wiki.gnome.org/Foundation/CodeOfConduct

Obrigado por fazer parte do projeto GNOME. Valorizamos a sua participação e queremos que todos tenham uma experiência agradável e gratificante. Consequentemente, espera-se que todos os participantes sigam este Código de Conduta e mostrem respeito, compreensão e consideração uns pelos outros. Obrigado por ajudar a tornar esta comunidade acolhedora e amigável para todos.

## Escopo

Este Código de Conduta se aplica a todos os espaços da comunidade GNOME, incluindo, mas não se limitando a:

- Sistemas de rastreamento de problemas - bugzilla.gnome.org
- Documentação e tutoriais - developer.gnome.org
- Repositórios de código - git.gnome.org e gitlab.gnome.org
- Listas de discussão - mail.gnome.org
- Wikis - wiki.gnome.org
- Bate-papo e fóruns - irc.gnome.org, discourse.gnome.org, canais GNOME no Telegram, e grupos e canais GNOME em Matrix.org (incluindo pontes para canais GNOME no IRC)
- Espaços comunitários hospedados na infraestrutura do gnome.org
- Quaisquer outros canais ou grupos que existam para discutir as atividades do projeto GNOME
- Todos os locais de eventos e espaços associados, incluindo conferências, hackfests, festas de lançamento, workshops e outros pequenos eventos
- Todas as áreas relacionadas aos locais do evento: salas de exibição de fornecedores, áreas de equipe e refeições, infraestrutura de conexão como passarelas, corredores, elevadores e escadas
- Eventos de patrocinador, seja no local ou fora dele
- Eventos privados fora do local que envolvem um ou mais participantes
- Conversas privadas ocorrendo em hotéis oficiais da conferência

Canais de comunicação e conversas privadas que normalmente estão fora do escopo podem ser considerados no escopo se um participante do GNOME estiver sendo perseguido ou assediado. Conversas de mídia social podem ser consideradas dentro do escopo se o incidente ocorreu sob uma hashtag de evento GNOME, ou quando uma conta oficial do GNOME na mídia social é marcada, ou dentro de qualquer outra discussão sobre GNOME. A Fundação GNOME reserva-se o direito de tomar medidas contra comportamentos que acontecem em qualquer contexto, se forem considerados relevantes para o projeto GNOME e seus participantes.

Todos os participantes dos espaços da comunidade GNOME estão sujeitos ao Código de Conduta. Isso inclui membros do conselho da Fundação GNOME, patrocinadores corporativos e funcionários pagos. Isso também inclui voluntários, mantenedores, líderes, contribuidores, revisores de contribuições, repórteres de questões, usuários do GNOME e qualquer pessoa que participe de discussões nos espaços da comunidade GNOME. Para eventos presenciais, isso também inclui todos os participantes, expositores, fornecedores, palestrantes, palestrantes, organizadores, funcionários e voluntários.

## Relatando um Incidente

Se você acredita que alguém está violando o Código de Conduta ou tem qualquer outra preocupação, [entre em contato com o comitê do Código de Conduta](https://wiki.gnome.org/Foundation/CodeOfConduct/ReporterGuide).

## Nossos Padrões

A comunidade GNOME se dedica a fornecer uma experiência positiva para todos, independentemente de:

- idade
- tamanho do corpo
- casta
- cidadania
- deficiência
- grau de escolaridade
- etnia
- status familiar
- expressão de gênero
- identidade de gênero
- informação genética
- status de imigração
- nível de experiência
- nacionalidade
- aparência pessoal
- gravidez
- raça
- religião
- características sexuais
- orientação sexual
- identidade sexual
- status socioeconômico
- tribo
- status de veterano

### Diretrizes da Comunidade

Exemplos de comportamento que contribuem para a criação de um ambiente positivo incluem:

- **Ser amigável.** Use uma linguagem acolhedora e inclusiva.

- **Ser empático.** Respeite os diferentes pontos de vista e experiências.

- **Ser respeitoso.** Quando discordamos, o fazemos de maneira educada e construtiva.

- **Ser atencioso.** Lembre-se de que as decisões costumam ser uma escolha difícil entre prioridades concorrentes. Concentre-se no que é melhor para a comunidade. Mantenha as discussões sobre as escolhas de tecnologia construtivas e respeitosas.

- **Ser paciente e generoso.** Se alguém pede ajuda é porque precisa. Quando houver documentação que responda à pergunta, educadamente indique-a. Se a pergunta não estiver relacionada ao assunto, sugira um espaço online mais apropriado para buscar ajuda.

- **Tentar ser conciso.** Leia a discussão antes de comentar para não repetir um ponto que foi feito.

### Comportamento Inapropriado

Os membros da comunidade solicitados a interromper qualquer comportamento impróprio devem obedecer imediatamente.

Queremos que todos os participantes da comunidade GNOME tenham a melhor experiência possível. Para deixar claro o que isso significa, fornecemos uma lista de exemplos de comportamentos que são inadequados para os espaços da comunidade GNOME:

- **Intimidação, perseguição ou perseguição deliberada.**

- **Interrupção sustentada de discussões, palestras ou outros eventos online.** A interrupção sustentada de eventos, discussões online ou reuniões, incluindo palestras e apresentações, não será tolerada. Isso inclui "falar sobre" ou "reclamar" dos palestrantes ou influenciar as ações da multidão que causam hostilidade nas sessões do evento. A interrupção sustentada também inclui o consumo de álcool em excesso, o uso excessivo de drogas recreativas ou o incentivo a outras pessoas.

- **Assédio de pessoas que não bebem álcool ou outras substâncias legais.** Não toleramos comentários depreciativos sobre aqueles que se abstêm de álcool ou outras substâncias legais. Não toleramos forçar as pessoas a beber, falar sobre sua abstinência ou preferências com outras pessoas, ou pressioná-las a beber – fisicamente ou por meio de zombarias.

- **Linguagem sexista, racista, homofóbica, transfóbica, capacitista ou de outra forma excludente.** Isso inclui referir-se deliberadamente a alguém por um gênero com o qual não se identifica e/ou questionar a legitimidade da identidade de gênero de um indivíduo. Se você não tem certeza se uma palavra é depreciativa, não a use. Isso também inclui discriminação sutil e/ou indireta repetida.

- **Atenção sexual indesejada ou comportamento que contribui para um ambiente sexualizado.** Isso inclui comentários sexualizados, piadas ou imagens em interações, comunicações ou materiais de apresentação, bem como toques, apalpadelas ou avanços sexuais inadequados. Os patrocinadores não devem usar imagens, atividades ou outros materiais sexualizados. A equipe de organização da reunião e outros organizadores voluntários não devem usar roupas/uniformes/fantasias sexualizados ou criar um ambiente sexualizado.

- **Contato físico indesejado.** Isso inclui tocar uma pessoa sem permissão, incluindo áreas sensíveis como o cabelo, barriga de gestante, dispositivo de mobilidade (cadeira de rodas, scooter etc.) ou tatuagens. Isso também inclui bloquear fisicamente ou intimidar outra pessoa. O contato físico ou contato físico simulado (como emojis como "beijo") sem consentimento afirmativo não é aceitável. Isso inclui o compartilhamento ou distribuição de imagens ou textos sexualizados.

- **Violência ou ameaças de violência.** Violência e ameaças de violência não são aceitáveis ​​– online ou offline. Isso inclui o incitamento à violência contra qualquer indivíduo, incluindo o incentivo a uma pessoa cometer autoagressão. Isso também inclui postar ou ameaçar postar informações de identificação pessoal de outras pessoas ("doxxing") online.

- **Influenciar ou encorajar o comportamento impróprio.** Se você influenciar ou encorajar outra pessoa a violar o Código de Conduta, poderá enfrentar as mesmas consequências que teria se tivesse violado o Código de Conduta.

- **Posse de uma arma ofensiva em um evento GNOME.** Isso inclui qualquer coisa considerada uma arma pelos organizadores do evento.

### Segurança versus Conforto

A comunidade GNOME prioriza a segurança das pessoas marginalizadas em relação ao conforto das pessoas privilegiadas, por exemplo, em situações que envolvem:

- "Reversismos", incluindo "racismo reverso", "sexismo reverso" e "cisfobia"
- Comunicação razoável de limites, como "me deixe em paz", "vá embora" ou "não vou discutir isso com você".
- Criticar comportamentos ou suposições racistas, sexistas, cissexistas ou de outra forma opressiva
- Comunicar limites ou criticar o comportamento opressor em um "tom" que você não acha adequado

Os exemplos listados acima não são contra o Código de Conduta. Se você tiver dúvidas sobre as afirmações acima, [leia nosso documento de Apoio à Diversidade](https://wiki.gnome.org/Foundation/CodeOfConduct/SupportingDiversity).

Esforços de divulgação e diversidade direcionados a grupos sub-representados são permitidos sob código de conduta. Por exemplo, um evento social para mulheres não seria classificado como estando fora do Código de Conduta de acordo com esta disposição.

As expectativas básicas de conduta não são cobertas pela "cláusula de reversismo" e seriam aplicadas independentemente da demografia dos envolvidos. Por exemplo, a discriminação racial não será tolerada, independentemente da raça dos envolvidos. Nem seria tolerada atenção sexual indesejada, independentemente do gênero ou orientação sexual de alguém. Os membros da nossa comunidade têm o direito de esperar que os participantes do projeto cumpram esses padrões.

Se um participante se envolver em um comportamento que viole este código de conduta, o comitê do Código de Conduta do GNOME pode tomar as medidas que julgar apropriadas. Exemplos de consequências são descritos no [Guia de Procedimentos do Comitê](https://wiki.gnome.org/Foundation/CodeOfConduct/CommitteeProcedures).

## Procedimento para Lidar com Incidentes

- [Guia do Relatante](https://wiki.gnome.org/Foundation/CodeOfConduct/ReporterGuide)

- [Procedimentos do Moderador](https://wiki.gnome.org/Foundation/CodeOfConduct/ModeratorProcedures)

- [Guia de Procedimentos do Comitê](https://wiki.gnome.org/Foundation/CodeOfConduct/CommitteeProcedures)

## Licença

O Código de Conduta do GNOME está licenciado sob uma [Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada](https://creativecommons.org/licenses/by-sa/3.0/deed.pt_BR).

![](https://licensebuttons.net/l/by-sa/3.0/88x31.png)
<!-- TODO: Adicionar alt-text "Licença Creative Commons" -->

## Atribuição

O Código de Conduta do GNOME foi bifurcado a partir da política de exemplo da [wiki de Feminismo Geek, criada pela Iniciativa Ada e outros voluntários](http://geekfeminism.wikia.com/wiki/Conference_anti-harassment/Policy), que está sob uma licença Creative Commons Zero.

Linguagem adicional foi incorporada e modificada a partir dos seguintes Códigos de Conduta:

- O [Código de Conduta Citizen](http://citizencodeofconduct.org/) está licenciado pela [Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada](https://creativecommons.org/licenses/by-sa/3.0/deed.pt_BR).

- O [template de Code of Conduct](https://github.com/sagesharp/code-of-conduct-template/) está licenciado com [Licença Creative Commons Atribuição 3.0 Não Adaptada](https://creativecommons.org/licenses/by/3.0/deed.pt_BR) da [Otter Tech](https://otter.technology/code-of-conduct-training)

- [Pacto de Colaboradores versão 1.4](https://www.contributor-covenant.org/pt/version/1/4/code-of-conduct/) (licenciado [CC BY 4.0](https://github.com/EthicalSource/contributor_covenant/blob/release/LICENSE.md))

- O [Código de Conduta da Carpintaria de Dados](https://docs.carpentries.org/topic_folders/policies/index_coc.html) é licenciado pela [Licença Creative Commons Atribuição 4.0](https://creativecommons.org/licenses/by/4.0/deed.pt_BR)

- O [Código de Conduta do Projeto Django](https://www.djangoproject.com/conduct/) está licenciado sob uma [Licença Creative Commons Atribuição 3.0 Não Adaptada](https://creativecommons.org/licenses/by/3.0/deed.pt_BR)

- [Código de Conduta do Fedora](https://docs.fedoraproject.org/pt_BR/project/code-of-conduct/)

- [Política de Anti-assédio do Feminismo Geek](https://geekfeminism.wikia.org/wiki/Conference_anti-harassment/Policy) que está sob uma [licença Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/deed.pt_BR)

- [Código de conduta anterior da Fundação GNOME](https://wiki.gnome.org/action/recall/Foundation/CodeOfConduct/Old)

- [Código de conduta do Slack para LGBTQ na Technology](https://lgbtq.technology/coc.html) licenciado sob a [Creative Commons Zero](https://creativecommons.org/publicdomain/zero/1.0/deed.pt_BR)

- [Diretrizes de participação na comunidade Mozilla](https://www.mozilla.org/pt-BR/about/governance/policies/participation/) está licenciada sob a [Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada](https://creativecommons.org/licenses/by-sa/3.0/deed.pt_BR)

- [Código de conduta dos mentores Python](http://pythonmentors.com/)

- [Código de Conduta da Comunidade do Speak Up!](http://web.archive.org/web/20141109123859/http://speakup.io/coc.html), licenciado sob uma [Licença Creative Commons Atribuição-CompartilhaIgual 3.0 Não Adaptada](https://creativecommons.org/licenses/by-sa/3.0/deed.pt_BR)
